export async function onRequest(ctx){
    let pwd = await ctx.env.HEATHENS.get('PASSWORD');
    return new Response(`Howdy, ${pwd}!`);
}
